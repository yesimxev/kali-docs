---
title: NetHunter Audio
description:
icon:
weight:
author: ["yesimxev",]
---

This modules enables Audio in KeX session.

![](nethunter-audio.png)
